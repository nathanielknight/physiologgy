import datetime
import typing as ty
import unittest

from physiologgy import average, model


def weights_days_back(
    weights: ty.Iterable[float]
) -> ty.Iterable[model.WeightEntry]:
    today = datetime.date.today()
    for daysback, weight in enumerate(weights):
        date = today + datetime.timedelta(days=-daysback)
        yield model.WeightEntry(date=date, weight=weight)


class TestAveraging(unittest.TestCase):
    def test_avg_of_constant_is_constant(self):
        weights = weights_days_back(100.0 for _ in range(20))
        averages = list(average.weighted(weights))
        for (weight, avg) in averages:
            self.assertEqual(avg, 100.0)

    def test_avg_of_increase_increases(self):
        weights = weights_days_back(100.0 - n for n in range(20))
        averages = list(average.weighted(weights))
        for ((w1, a1), (w2, a2)) in zip(averages[:-1], averages[1:]):
            self.assertGreater(w2.weight, w1.weight)
            self.assertGreater(
                w2.weight, a2
            )  # average increaes slower than weight

    def test_avg_of_decrease_decreases(self):
        weights = weights_days_back(100.0 + n for n in range(20))
        averages = list(average.weighted(weights))
        for ((w1, a1), (w2, a2)) in zip(averages[:-1], averages[1:]):
            self.assertLess(w2.weight, w1.weight)
            self.assertLess(
                w2.weight, a2
            )  # average decreases slower than weight

