import contextlib
import datetime
import pathlib
import tempfile
import unittest

from physiologgy import repository
from physiologgy import model


class TestWeightRepository(unittest.TestCase):
    @staticmethod
    @contextlib.contextmanager
    def tmprepo():
        with tempfile.TemporaryDirectory() as tmpdir:
            dbpath = pathlib.Path(tmpdir) / "db.sqlite3"
            yield repository.WeightRepository(dbpath)

    def test_save_and_load(self):
        eg_wts = [
            model.WeightEntry(
                date=datetime.date(2010, 10, n + 1), weight=(100) + n
            )
            for n in range(10)
        ]
        with self.tmprepo() as repo:
            for wt in eg_wts:
                repo.save(wt)
            self.assertEqual(list(reversed(eg_wts)), repo.all())

