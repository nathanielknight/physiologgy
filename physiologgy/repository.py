"""Save and load log entries"""
import contextlib
import datetime
import pathlib
import sqlite3
import typing as ty

from . import model


class WeightRepository:
    INIT = """
    CREATE TABLE IF NOT EXISTS weight_log
    (
        date TEXT NOT NULL,
        weight NUMERIC NOT NULL
    )
    """
    ALL = "SELECT date, weight FROM weight_log ORDER BY date"
    ADD = "INSERT INTO weight_log (date, weight) VALUES (?, ?)"
    DELETE = "DELETE FROM weight_log WHERE date = ? AND weight = ?"

    def __init__(self, dbpath: pathlib.Path):
        self._dbpath = dbpath
        with self._get_dbcursor() as cursor:
            cursor.execute(self.INIT)

    @staticmethod
    def parserow(row: ty.Tuple[str, float]) -> model.WeightEntry:
        date_source, weight = row
        date = datetime.date.fromisoformat(date_source)
        return model.WeightEntry(date=date, weight=weight)

    @contextlib.contextmanager
    def _get_dbcursor(self) -> ty.Iterator[sqlite3.Cursor]:
        with sqlite3.connect(str(self._dbpath)) as connection:
            cursor = connection.cursor()
            yield cursor

    def save(self, wt: model.WeightEntry) -> None:
        with self._get_dbcursor() as cursor:
            params = (wt.date.isoformat(), wt.weight)
            cursor.execute(self.ADD, params)

    def delete(self, wt: model.WeightEntry) -> None:
        params = (wt.date.isoformat(), wt.weight)
        with self._get_dbcursor() as cursor:
            cursor.execute(self.DELETE, params)

    def all(self) -> ty.List[model.WeightEntry]:
        with self._get_dbcursor() as cursor:
            rows = cursor.execute(self.ALL).fetchall()
        weights = map(self.parserow, rows)
        return sorted(weights, key=lambda w: w.date, reverse=True)

    def last(self) -> ty.Optional[model.WeightEntry]:
        entries = self.all()
        if entries:
            return max(self.all(), key=lambda w: w.date)
        else:
            return None


class ExerciseRepository:
    INIT = """
    CREATE TABLE IF NOT EXISTS exercise_log
    (
        date TEXT NOT NULL,
        rank NUMERIC NOT NULL
    )
    """
    ALL = "SELECT date, rank FROM exercise_log ORDER BY date"
    ADD = "INSERT INTO exercise_log (date, rank) VALUES (?, ?)"
    DELETE = "DELETE FROM exercise_log WHERE date = ? AND rank = ?"

    def __init__(self, dbpath: pathlib.Path):
        self._dbpath = dbpath
        with self._get_dbcursor() as cursor:
            cursor.execute(self.INIT)

    @staticmethod
    def parserow(row: ty.Tuple[str, int]) -> model.ExerciseEntry:
        date_source, rank = row
        date = datetime.date.fromisoformat(date_source)
        return model.ExerciseEntry(date=date, rank=rank)

    @contextlib.contextmanager
    def _get_dbcursor(self) -> ty.Iterator[sqlite3.Cursor]:
        with sqlite3.connect(str(self._dbpath)) as connection:
            cursor = connection.cursor()
            yield cursor

    def all(self) -> ty.List[model.ExerciseEntry]:
        with self._get_dbcursor() as cursor:
            rows = cursor.execute(self.ALL).fetchall()
        exercises = map(self.parserow, rows)
        return sorted(exercises, key=lambda w: w.date, reverse=True)

    def save(self, ex: model.ExerciseEntry) -> None:
        with self._get_dbcursor() as cursor:
            params = (ex.date.isoformat(), ex.rank)
            cursor.execute(self.ADD, params)

    def delete(self, ex: model.ExerciseEntry) -> None:
        params = (ex.date.isoformat(), ex.rank)
        with self._get_dbcursor() as cursor:
            cursor.execute(self.DELETE, params)

    def last(self) -> ty.Optional[model.ExerciseEntry]:
        entries = self.all()
        if entries:
            return max(self.all(), key=lambda w: w.date)
        else:
            return None
