import itertools
import typing as ty

from . import model


def weighted(
    weights: ty.List[model.WeightEntry], smoothing_factor: float = 0.1
) -> ty.Iterable[ty.Tuple[model.WeightEntry, float]]:
    weights = list(
        itertools.islice(sorted(weights, key=lambda w: w.date), 0, 20)
    )
    smoothed = None
    for weight in weights:
        if smoothed:
            smoothed = smoothed + smoothing_factor * (weight.weight - smoothed)
            yield (weight, smoothed)
        else:
            smoothed = weight.weight
            yield (weight, smoothed)
