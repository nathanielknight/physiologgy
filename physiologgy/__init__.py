import datetime
import logging
import os
import pathlib
import typing as ty

import flask
import pytz

from . import exercise, index, model, repository


DBPATH_SRC = os.getenv("PHYSIOLOGGY_DBPATH", "./db.sqlite3")
DBPATH = pathlib.Path(DBPATH_SRC)
DEFAULT_TIMEZONE = pytz.timezone("Canada/Pacific")

WEIGHT_REPO = repository.WeightRepository(DBPATH)
EXERCISE_REPO = repository.ExerciseRepository(DBPATH)

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


app = flask.Flask("physiologgy")


def default_date() -> datetime.date:
    utcnow = datetime.datetime.utcnow()
    return DEFAULT_TIMEZONE.fromutc(utcnow).date()


@app.route("/", methods=["GET"])
def root() -> ty.Any:
    indexrows = index.rowdata(exercise_repo=EXERCISE_REPO, weight_repo=WEIGHT_REPO)
    return flask.render_template("index.html", rows=indexrows)


@app.route("/weight", methods=["GET", "POST"])
def weight() -> ty.Any:
    if flask.request.method == "GET":
        last_entry = WEIGHT_REPO.last()
        if last_entry is not None:
            default_wt = str(last_entry.weight)
        else:
            default_wt = ""
        return flask.render_template(
            "weight.html", default_wt=default_wt, default_date=default_date()
        )
    else:
        date_src = str(flask.request.form.get("date"))
        weight_src = str(flask.request.form.get("weight"))
        entry = model.WeightEntry(
            date=datetime.date.fromisoformat(date_src),
            weight=float(weight_src),
        )
        WEIGHT_REPO.save(entry)
        index_url = flask.url_for("root")
        return flask.redirect(index_url, code=303)


@app.route("/delete_weight", methods=["POST"])
def delete_weight() -> ty.Any:
    date_src = str(flask.request.form.get("date"))
    weight_src = str(flask.request.form.get("weight"))
    entry = model.WeightEntry(
        date=datetime.date.fromisoformat(date_src),
        weight=float(weight_src),
    )
    WEIGHT_REPO.delete(entry)
    return flask.redirect(flask.url_for("root"), code=303)


@app.route("/exercise", methods=["GET", "POST"], endpoint="exercise")
def exercise_view() -> ty.Any:
    if flask.request.method == "GET":
        last_entry = EXERCISE_REPO.last()
        if last_entry is not None:
            min_rank, max_rank = exercise.possible_ranks(last_entry.rank)
            default_rank = last_entry.rank
        else:
            min_rank, max_rank = exercise.possible_ranks(1)
            default_rank = 1
        regimens = [exercise.regimen(rank) for rank in range(min_rank, max_rank + 1)]
        regtitles = exercise.Regimen.titles()
        return flask.render_template(
            "exercise.html",
            regimens=regimens,
            default_rank=default_rank,
            regtitles=regtitles,
            default_date=default_date(),
        )
    else:
        date_src = flask.request.form.get("date")
        rank_src = flask.request.form.get("rank")
        assert date_src is not None and rank_src is not None
        entry = model.ExerciseEntry(
            date=datetime.date.fromisoformat(date_src), rank=int(rank_src)
        )
        EXERCISE_REPO.save(entry)
        return flask.redirect(flask.url_for("root"), code=303)


@app.route("/delete_exercise", methods=["POST"])
def delete_exercise() -> ty.Any:
    date_src = str(flask.request.form.get("date"))
    rank_src = str(flask.request.form.get("rank"))
    entry = model.ExerciseEntry(
        date=datetime.date.fromisoformat(date_src),
        rank=int(rank_src),
    )
    EXERCISE_REPO.delete(entry)
    return flask.redirect(flask.url_for("root"), code=303)
