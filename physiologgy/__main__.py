from . import app, log


if __name__ == "__main__":
    log.info("Listening on port 8081")
    app.run(host="localhost", port=8081)
