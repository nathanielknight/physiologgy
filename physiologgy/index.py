"Query to populate the table on the main page"
import datetime
import itertools
import typing as ty

from . import repository, average


class IndexRow(ty.NamedTuple):
    date: datetime.date
    weight: ty.Optional[float]
    weight_average: ty.Optional[float]
    exercise_rank: ty.Optional[int]


def rowdata(
    exercise_repo: repository.ExerciseRepository,
    weight_repo: repository.WeightRepository,
) -> ty.Iterable[IndexRow]:
    weight_entries = {
        w.date: w for w in weight_repo.all()
    }
    averaged_weights = {
        w[0].date: w[1]
        for w in average.weighted(list(weight_entries.values()))
    }
    exercise_entries = {
        e.date: e for e in exercise_repo.all()
    }
    dates = sorted(
        set(weight_entries.keys()).union(exercise_entries.keys()), reverse=True
    )
    for date in itertools.islice(dates, 25):
        weight_entry = weight_entries.get(date)
        weight_average = averaged_weights.get(date)
        exercise_entry = exercise_entries.get(date)
        yield IndexRow(
            date=date,
            weight=getattr(weight_entry, "weight", None),
            weight_average=weight_average,
            exercise_rank=getattr(exercise_entry, "rank", None),
        )
