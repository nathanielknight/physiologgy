"""Representations of log entries.

Deliberately minimal so that other parts of the application can depend on it.
"""

import datetime
import typing as ty


class WeightEntry(ty.NamedTuple):
    date: datetime.date  # The date the measurement was taken
    weight: float  # Weight in Lbs (because that's the scale I have)


class ExerciseEntry(ty.NamedTuple):
    date: datetime.date  # The date the measurement was taken
    rank: int  # The level of exercise performed
