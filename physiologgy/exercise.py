"Exercise regimen"
import math
import typing as ty


class Regimen(ty.NamedTuple):
    rank: int
    bends: int
    squats: int
    pushups: int
    situps: int
    leg_lifts: int
    lunges: int
    stride_sets: int
    sun_salutations: int

    @classmethod
    def titles(cls) -> ty.List[str]:
        return [fld.capitalize().replace("_", " ") for fld in cls._fields]


MULTIPLIERS = {
    "bends": 1.5,
    "squats": 1.5,
    "pushups": 1.2,
    "situps": 1.5,
    "leg_lifts": 1.5,
    "lunges": 1.5,
    "stride_sets": 0.4,
    "sun_salutations": 0.25,
}


def regimen(rank: int) -> Regimen:
    nums = {
        name: math.ceil(rank * factor) for name, factor in MULTIPLIERS.items()
    }
    return Regimen(rank=rank, **nums)


def possible_ranks(last_rank: int) -> ty.Tuple[int, int]:
    "Possible ranks for today's exercise based on yesterday's exercise"
    max_rank = last_rank + 1
    min_rank = max(1, last_rank - 1)
    return (min_rank, max_rank)
