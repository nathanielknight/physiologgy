FROM python:3.8-slim-buster

ENV PHYSIOLOGGY_DBPATH "/data/db.sqlite"

# Install Python modules
ADD . .
RUN python3 -m pip install --upgrade pip && python3 -m pip install -r requirements.txt

CMD ["python", "-m", "waitress", "--listen=0.0.0.0:8081", "physiologgy:app"]

